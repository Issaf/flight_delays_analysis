# Flight Delays Analysis

A quick analysis of 2008 US flight delays using Tableau. After some data cleaning and formatting with python, I made a story illustrating different factors that could cause delays.
