import pandas as pd

df = pd.read_csv("2008.csv")

rows = [0,1,2,3,5,8,11,12,14,15,16,17,18]

df2 = df.iloc[:,rows]
df2 = df2.rename(index=str, columns={"DayofMonth": "Day"})
df2['DepDate'] = pd.to_datetime(df2.iloc[:,[0,1,2]])
df3 = df2.iloc[:,range(3,len(df2.columns))]


def correct_time(number):
    s = str(number)
    if number < 10:
        return '00:0'+ s
    elif number < 99:
        return '00:'+ s
    elif number < 999:
       
        return '0'+s[0]+':'+s[1:]
    else:
        return s[0:2]+':'+s[2:]


def daysofweek(day):
	if day == 1: return 'Monday'
	elif day == 2: return 'Tuesday'
	elif day == 3: return 'Wednesday'
	elif day == 4: return 'Thursday'
	elif day == 5: return 'Friday'
	elif day == 6: return 'Saturday'
	elif day == 7: return 'Sunday'
    

df3.CRSDepTime = df3.CRSDepTime.apply(correct_time)
df3.DayOfWeek = df3.DayOfWeek.apply(daysofweek)

import datetime as dt

df3.CRSDepTime = pd.to_datetime(df3.CRSDepTime,format= '%H:%M').dt.time
def periods(time):
    if time >= dt.time(5,0) and time < dt.time(12,0):
        return 'Morning'
    elif time >= dt.time(12,0) and time < dt.time(17,30):
        return 'Afternoon'
    elif time >= dt.time(17,30) and time < dt.time(21,0):
        return 'Evening'
    elif time >= dt.time(21,0) or time < dt.time(5,0):
        return 'Night'
    
df3['DayPeriods'] = df3.CRSDepTime.apply(periods)



df3.to_csv('2008clean.csv')
